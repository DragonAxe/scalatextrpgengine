lazy val root = (project in file(".")).
  settings(
    name := "FunctionalTextRPGEngine",
    version := "1.0",
    scalaVersion := "2.11.7",
    scalaSource in Compile := baseDirectory.value / "src",
    scalaSource in Test := baseDirectory.value / "test-src"//,
    //mainClass in Compile := Some("src.Main")
  )

scalacOptions += "-deprecation"
