class Navigation(area: Area, navigationKeyword: String) {
  def getArea = area
  def getKeyword = navigationKeyword
}
