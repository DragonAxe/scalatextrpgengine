import scala.io.StdIn.readLine

object SimpleGame {

  def gameLoop(state:AbstractGameState):Unit = {
    // Read
    print(">> ")
    val userIn = readLine()
    // Evaluate
    val (evalResponse, evalState) = eval(userIn, state)
    // Print
    println(evalResponse)
    if (evalResponse != "terminate") gameLoop(evalState)
  }

  def eval(input:String, state:AbstractGameState):(String, AbstractGameState) = {
    val s = input.toLowerCase

    if (s == "exit") ("terminate", state)

    else if (s == "where am i?") (state.getCurrentArea.getName, state)

    else if (s.startsWith("navigate")) ("Switching to: ...", state)

    else ("There's no command with that name: "+s, state)
  }

}
